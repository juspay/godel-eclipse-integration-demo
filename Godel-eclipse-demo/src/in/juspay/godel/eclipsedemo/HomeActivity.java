package in.juspay.godel.eclipsedemo;

import in.juspay.godel.ui.JuspayBrowserFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

public class HomeActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        
        JuspayBrowserFragment browserFragment = new JuspayBrowserFragment(){};

        // setup the initial parameter(s) for the browser fragment
        Bundle args = new Bundle();
        args.putString("url", "https://www.amazon.in");
        args.putString("merchantId", "amazon");
        args.putString("clientId", "amazon_adroid");
        args.putString("transactionId", "1231231230");
        browserFragment.setArguments(args);
        
        FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, browserFragment);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.commit();

    }
}
